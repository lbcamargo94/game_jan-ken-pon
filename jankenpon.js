// Lógica JS
// Aluno Kenzie Academy
// By: lbcamargo

// ---------------------------------------------------------------------------------------------
// Função Aleatória

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
}

// Função Aleatória
// ---------------------------------------------------------------------------------------------
// Função Reset

function setReset() {
    location.reload(true);
}

// Função Reset
// ---------------------------------------------------------------------------------------------
// Função Escolha Aleatória do Computador

const jogadaComputer = () => {
    let opcoes = ["Pedra", "Papel", "Tesoura"];
    let escolha = opcoes[getRandomInt(0, 3)];
    return (escolha)
}

// Função Escolha Aleatória do Computador
// ---------------------------------------------------------------------------------------------
// Função Clicar no Botão Play

const playGame = () => {}

// Função Clicar no Botão Play
// ---------------------------------------------------------------------------------------------
// Função Player escolhe Card Pedra

const jogadorPedra = () => {
    let Computer = jogadaComputer();
    console.log(Computer);
    if (Computer === "Tesoura") {
        let venceu = document.getElementById("container__Resultado");
        let resultado = document.createElement("h3");
        resultado.className = "textResultado";
        venceu.appendChild(resultado);
        let textnode = document.createTextNode("Parabéns, Você Venceu!");
        resultado.appendChild(textnode);
    }
    if (Computer === "Papel") {
        let perdeu = document.getElementById("container__Resultado");
        let resultado = document.createElement("h3");
        resultado.className = "textResultado";
        perdeu.appendChild(resultado);
        let textnode = document.createTextNode("Não foi dessa vez, Você Perdeu!");
        resultado.appendChild(textnode);
    }
    if (Computer === "Pedra") {
        let empate = document.getElementById("container__Resultado");
        let resultado = document.createElement("h3");
        resultado.className = "textResultado";
        empate.appendChild(resultado);
        let textnode = document.createTextNode("Temos um empate ... , Tente novamente!");
        resultado.appendChild(textnode);
    }
}

// Função Player escolhe Card Pedra
// ---------------------------------------------------------------------------------------------

// Função Player escolhe Card Papel
const jogadorPapel = () => {
    let Computer = jogadaComputer();
    console.log(Computer);
    if (Computer === "Pedra") {
        let venceu = document.getElementById("container__Resultado");
        let resultado = document.createElement("h3");
        resultado.className = "textResultado";
        venceu.appendChild(resultado);
        let textnode = document.createTextNode("Parabéns, Você Venceu!");
        resultado.appendChild(textnode);
    }
    if (Computer === "Tesoura") {
        let perdeu = document.getElementById("container__Resultado");
        let resultado = document.createElement("h3");
        resultado.className = "textResultado";
        perdeu.appendChild(resultado);
        let textnode = document.createTextNode("Não foi dessa vez, Você Perdeu!");
        resultado.appendChild(textnode);
    }
    if (Computer === "Papel") {
        let empate = document.getElementById("container__Resultado");
        let resultado = document.createElement("h3");
        resultado.className = "textResultado";
        empate.appendChild(resultado);
        let textnode = document.createTextNode("Temos um empate ... , Tente novamente!");
        resultado.appendChild(textnode);
    }
}

// Função Player escolhe Card Papel
// ---------------------------------------------------------------------------------------------

// Função Player escolhe Card Tesoura

const jogadorTesoura = () => {
    let Computer = jogadaComputer();
    console.log(Computer);
    if (Computer === "Papel") {
        let venceu = document.getElementById("container__Resultado");
        let resultado = document.createElement("h3");
        resultado.className = "textResultado";
        venceu.appendChild(resultado);
        let textnode = document.createTextNode("Parabéns, Você Venceu!");
        resultado.appendChild(textnode);
    }
    if (Computer === "Pedra") {
        let perdeu = document.getElementById("container__Resultado");
        let resultado = document.createElement("h3");
        resultado.className = "textResultado";
        perdeu.appendChild(resultado);
        let textnode = document.createTextNode("Não foi dessa vez, Você Perdeu!");
        resultado.appendChild(textnode);
    }
    if (Computer === "Tesoura") {
        let empate = document.getElementById("container__Resultado");
        let resultado = document.createElement("h3");
        resultado.className = "textResultado";
        empate.appendChild(resultado);
        let textnode = document.createTextNode("Temos um empate ... , Tente novamente!");
        resultado.appendChild(textnode);
    }
}

// Função Player escolhe Card Tesoura
// ---------------------------------------------------------------------------------------------